FROM node:carbon

#Create app directory
WORKDIR /usr/src/app

#Install app dependencies
#A wildcard is used to ensure both package.json AND package-lock.json are copied
# where avaialable (npm@5+)
COPY package*.json ./

RUN npm install
# If you are builidng your for production
# RUN npm install --only=production

# Bundle app source
COPY . . 

EXPOSE 808
CMD [ "npm", "start" ]
